﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZRegex.Net
{
    public class Pair<L, R>
    {
        public L Left { get; set; }
        public R Right { get; set; }
        public static Pair<L, R> Of(L left, R right)
        {
            return new Pair<L, R>() { Left = left, Right = right };
        }
    }
}
