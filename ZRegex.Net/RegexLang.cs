﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZRegex.Net
{
    // 'character' in regex lang
    public abstract class RegexChar
    {
    }
    // 'string' in regex lang
    public class RegexCharString
    {
        public IList<RegexChar> Chars { get; set; }
        public RegexCharString()
        {
            this.Chars = new List<RegexChar>();
        }
        public RegexCharString(RegexChar c)
            : this()
        {
            Chars.Add(c);
        }
        public virtual void AddChar(RegexChar c)
        {
            Chars.Add(c);
        }
        public virtual int Count
        {
            get { return Chars.Count; }
        }
        public virtual bool Contains(RegexChar c)
        {
            for (var i = 0; i < Count; ++i)
            {
                if (this[i].Equals(c))
                {
                    return true;
                }
            }
            return false;
        }
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj is RegexCharString)
            {
                var other = (RegexCharString)obj;
                if (this.Count != other.Count)
                {
                    return false;
                }
                for (var i = 0; i < this.Count; ++i)
                {
                    if (!this.Chars[i].Equals(other.Chars[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual RegexCharString SubString(int from, int end = -1)
        {
            var other = new RegexCharString();
            for (var i = from; i < (end >= 0 ? end : this.Count); ++i)
            {
                other.AddChar(this.Chars[i]);
            }
            return other;
        }

        public override int GetHashCode()
        {
            var sum = 0;
            for (var i = 0; i < Count; ++i)
            {
                sum += this[i].GetHashCode();
            }
            return sum;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            for (var i = 0; i < Count; ++i)
            {
                builder.Append(this[i]);
            }
            return builder.ToString();
        }

        public RegexChar this[int index]
        {
            get
            {
                return Chars[index];
            }
        }

        public virtual RegexCharString FromChar(RegexChar c)
        {
            throw new NotImplementedException();
        }

        public virtual RegexCharString AppendToNew(RegexChar c)
        {
            return AppendToNew(FromChar(c));
        }

        public virtual RegexCharString AppendToNew(RegexCharString str2)
        {
            var result = new RegexCharString();
            for (var i = 0; i < this.Count; ++i)
            {
                result.AddChar(this[i]);
            }
            for (var i = 0; i < str2.Count; ++i)
            {
                result.AddChar(str2[i]);
            }
            return result;
        }
    }
    // 'language' in regex lang
    public class RegexLang
    {
        protected ISet<RegexCharString> _stringSet { get; set; }
        public RegexLang()
        {
            this._stringSet = new HashSet<RegexCharString>();
        }
        public virtual void Add(RegexCharString str)
        {
            _stringSet.Add(str);
        }
        public virtual bool Contains(RegexCharString str)
        {
            return _stringSet.Contains(str);
        }
    }

    public class CharRegexChar : RegexChar
    {
        public char Char { get; set; }
        public CharRegexChar(char c)
            : base()
        {
            this.Char = c;
        }
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj is CharRegexChar)
            {
                return this.Char == ((CharRegexChar)obj).Char;
            }
            else
            {
                return false;
            }
        }
        public override string ToString()
        {
            return "" + Char;
        }
        public override int GetHashCode()
        {
            return Char.GetHashCode();
        }
    }

    public class StringRegexCharString : RegexCharString
    {
        public StringRegexCharString(string str)
            : base()
        {
            for (var i = 0; i < str.Length; ++i)
            {
                AddChar(new CharRegexChar(str[i]));
            }
        }
        public override RegexCharString FromChar(RegexChar c)
        {
            var str = new StringRegexCharString("");
            str.AddChar(c);
            return str;
        }

        public override RegexCharString AppendToNew(RegexCharString str2)
        {
            var result = new StringRegexCharString("");
            for (var i = 0; i < this.Count; ++i)
            {
                result.AddChar(this[i]);
            }
            for (var i = 0; i < str2.Count; ++i)
            {
                result.AddChar(str2[i]);
            }
            return result;
        }
    }

}

