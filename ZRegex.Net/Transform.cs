﻿using System;

namespace ZRegex.Net
{
	public abstract class Transform
	{
		public State From {get;set;}
		public State To {get;set;}
		public Transform(State from, State to){
			this.From = from;
			this.To = to;
		}
		public override string ToString ()
		{
			return string.Format ("{0}->{1}", From, To);
		}
		public abstract bool Match(RegexCharString str, int pos);
		public abstract Transform CopyNew();
	}
}

