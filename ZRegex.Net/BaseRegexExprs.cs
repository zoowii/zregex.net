﻿using System;
using ZRegex.Net;

namespace ZRegex.Net
{
    public class CharExpr : RegexExpr
    {
        public CharExpr(char c)
            : base()
        {
            var startState = new State();
            var endState = new State();
            var charTrans = new CharTransform(startState, endState, new CharRegexChar(c));
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(charTrans);
        }
        public override RegexExpr CreateInstance()
        {
            return new CharExpr('\0');
        }
        public static RegexExpr CreateUnionFromChars(string str)
        {
            if (str == null || str.Length < 1)
            {
                return new EmptyExpr();
            }
            RegexExpr result = null;
            for (var i = 0; i < str.Length; ++i)
            {
                if (i == 0)
                {
                    result = new CharExpr(str[i]);
                }
                else
                {
                    result = result.Union(new CharExpr(str[i]));
                }
            }
            return result;
        }
    }
    public class CharNotInRangeExpr:RegexExpr
    {
        public string OutRange {get;set;}
        public CharNotInRangeExpr(string outRange):base(){
            this.OutRange = outRange;
            var startState = new State();
            var endState = new State();
            var charTrans = new CharNotInRangeTransform(startState, endState, new StringRegexCharString(outRange));
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(charTrans);
        }
        public override RegexExpr CreateInstance()
        {
            return new CharNotInRangeExpr("");
        }
    }

    public class DigitExpr : RegexExpr
    {
        public DigitExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var digitTrans = new DigitTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(digitTrans);
        }
        public override RegexExpr CreateInstance()
        {
            return new DigitExpr();
        }
    }

    public class AnyCharExpr : RegexExpr
    {
        public AnyCharExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var digitTrans = new AnyCharTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(digitTrans);
        }
        public override RegexExpr CreateInstance()
        {
            return new AnyCharExpr();
        }
    }

    public class CharRangeExpr : RegexExpr
    {
        public CharRangeExpr(char start, char end)
            : base()
        {
            var startState = new State();
            var endState = new State();
            var trans = new CharRangeTransform(startState, endState, new CharRegexChar(start), new CharRegexChar(end));
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(trans);
        }
        public override RegexExpr CreateInstance()
        {
            return new CharRangeExpr('\0', '\0');
        }
    }

    public class AlphaExpr : RegexExpr
    {
        public AlphaExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var trans1 = new BigAlphaTransform(startState, endState);
            var trans2 = new LittleAlphaTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(trans1);
            this.Transforms.Add(trans2);
        }
        public override RegexExpr CreateInstance()
        {
            return new AlphaExpr();
        }
    }

    public class BigAlphaExpr : RegexExpr
    {
        public BigAlphaExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var trans = new BigAlphaTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(trans);
        }
        public override RegexExpr CreateInstance()
        {
            return new BigAlphaExpr();
        }
    }

    public class LittleAlphaExpr : RegexExpr
    {
        public LittleAlphaExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var trans = new LittleAlphaTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(trans);
        }
        public override RegexExpr CreateInstance()
        {
            return new LittleAlphaExpr();
        }
    }


    public class EmptyExpr : RegexExpr
    {
        public EmptyExpr()
            : base()
        {
            var startState = new State();
            var endState = new State();
            var emptyTrans = new EmptyTransform(startState, endState);
            this.StartState = startState;
            this.States.Add(startState);
            this.States.Add(endState);
            this.AcceptableStates.Add(endState);
            this.Transforms.Add(emptyTrans);
        }

        public override RegexExpr CreateInstance()
        {
            return new EmptyExpr();
        }
    }
}

