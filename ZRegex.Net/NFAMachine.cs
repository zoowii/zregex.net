﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace ZRegex.Net
{
    public class MatchGroupResult
    {
        public RegexCharString MatchedString { get; set; }
        public string GroupIdentity { get; set; }
        public override string ToString()
        {
            return string.Format("<{0},{1}>", MatchedString, GroupIdentity);
        }
    }
    public class MatchResults
    {
        public bool Matched { get; set; }
        public RegexCharString RemainingString { get; set; }
        public IList<MatchGroupResult> MatchedItems { get; set; }
        public MatchResults()
        {
            this.MatchedItems = new List<MatchGroupResult>();
        }
        public int Count
        {
            get
            {
                return this.MatchedItems.Count;
            }
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("[");
            for (var i = 0; i < MatchedItems.Count; ++i)
            {
                if (i == 0)
                {
                    builder.Append(",");
                }
                builder.Append(MatchedItems[i].ToString());
            }
            builder.Append("]");
            return builder.ToString();
        }
    }
    public class MatchResult
    {
        public bool Matched { get; set; }
        public RegexCharString LastMatchedString { get; set; } // last in accept state's matched string
        public State EndState { get; set; }
        public string EndGroupIdentity { get; set; }
        public override string ToString()
        {
            return string.Format("[MatchResult: Matched={0}, LastMatchedString={1}, EndState={2}]", Matched, LastMatchedString, EndState);
        }
        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (!(obj is MatchResult))
            {
                return false;
            }
            var other = obj as MatchResult;
            return this.Matched.Equals(other.Matched) && this.LastMatchedString.Equals(other.LastMatchedString);
        }

        public override int GetHashCode()
        {
            return Matched.GetHashCode() + LastMatchedString.GetHashCode();
        }

        public static MatchResult of(bool matched, RegexCharString str)
        {
            return new MatchResult() { Matched = matched, LastMatchedString = str };
        }
    }
    public class PathState
    {
        public RegexCharString Path { get; set; }
        public State State { get; set; }
    }
    public class NFAMachine
    {
        public IList<State> States { get; set; }
        public IList<Transform> Transforms { get; set; }
        public State StartState { get; set; }
        public IList<State> AcceptableStates { get; set; }
        public string GroupIdentity { get; set; }

        /**
         * from-state => transforms-from-the-state mapping dict
         */
        public Dictionary<State, ISet<Transform>> TransformMapping { get; set; }

        /**
         * build state transform mapping dict
         */
        public void Build()
        {
            TransformMapping = new Dictionary<State, ISet<Transform>>();
            foreach(var trans in Transforms)
            {
                if (!TransformMapping.ContainsKey(trans.From))
                {
                    TransformMapping[trans.From] = new HashSet<Transform>();
                }
                var transes = TransformMapping[trans.From];
                transes.Add(trans);
            }
        }

        public void MarkGroup(string group)
        {
            this.GroupIdentity = group;
            foreach (var state in States)
            {
                state.GroupIdentity = group;
            }
        }

        public void AddTransform(Transform trans)
        {
            this.Transforms.Add(trans);
        }

        public NFAMachine()
        {
            this.States = new List<State>();
            this.Transforms = new List<Transform>();
            this.AcceptableStates = new List<State>();
        }

        protected virtual ISet<State> GetStatesEmptyConnectedFrom(State state)
        {
            var result = new HashSet<State>();
            foreach (var trans in GetTransformsFrom(state))
            {
                if (trans is EmptyTransform)
                {
                    result.Add(trans.To);
                    foreach (var other in GetStatesEmptyConnectedFrom(trans.To))
                    {
                        result.Add(other);
                    }
                }
            }
            return result;
        }

        protected virtual ISet<Transform> GetTransformsFrom(State state)
        {
            if (TransformMapping != null)
            {
                if (TransformMapping.ContainsKey(state))
                {
                    return TransformMapping[state];
                }
                else
                {
                    return new HashSet<Transform>();
                }
            }
            var result = new HashSet<Transform>();
            foreach (var trans in Transforms)
            {
                if (trans.From == state)
                {
                    result.Add(trans);
                }
            }
            return result;
        }

        public virtual RegexCharString CreateEmptyCharString()
        {
            return new RegexCharString();
        }

        /**
         * match all occurances of the pattern
         */
        public virtual MatchResults MatchAll(RegexCharString str)
        {
            var matchResults = new MatchResults();
            var remaining = str;
            while (remaining.Count > 0)
            {
                var matchResult = Match(remaining);
                if (matchResult == null||matchResult.LastMatchedString==null)
                {
                    remaining = remaining.SubString(1);
                    continue;
                }
                var newRemaining = remaining.SubString(matchResult.LastMatchedString.Count);
                if (remaining.Count != newRemaining.Count)
                {
                    matchResults.MatchedItems.Add(new MatchGroupResult() { GroupIdentity = matchResult.EndGroupIdentity,
                        MatchedString = matchResult.LastMatchedString });
                }
                if (remaining.Equals(newRemaining))
                {
                    break;
                }
                else
                {
                    remaining = newRemaining;
                }
                if (matchResult.Matched)
                {
                    break;
                }
            }
            matchResults.RemainingString = remaining;
            matchResults.Matched = remaining.Count < 1;
            return matchResults;
        }

        public virtual MatchResult Match(RegexCharString str)
        {
            var queue = new List<PathState>();
            var initStates = GetStatesEmptyConnectedFrom(StartState);
            initStates.Add(StartState);
            foreach (var state in initStates)
            {
                queue.Add(new PathState() { State = state, Path = CreateEmptyCharString() });
            }
            int pos = 0;
            RegexCharString lastMatchedString = null;
            string lastStateGroup = null;
            State lastState = null;
            do
            {
                foreach (var item in queue)
                {
                    if (item.State.GroupIdentity != null) {
                        lastStateGroup = item.State.GroupIdentity;
                    }
                    lastState = item.State;
                    if (AcceptableStates.Contains(item.State))
                    {
                        lastMatchedString = item.Path;
                    }
                    if (item.Path.Equals(str) && AcceptableStates.Contains(item.State))
                    {
                        return new MatchResult() { LastMatchedString = lastMatchedString, Matched = true, EndState=lastState, EndGroupIdentity=lastStateGroup};
                    }
                }
                if (pos >= str.Count)
                {
                    break;
                }
                var newQueue = new List<PathState>();
                foreach (var item in queue)
                {
                    var transforms = GetTransformsFrom(item.State);
                    foreach (var trans in transforms)
                    {
                        if (trans.Match(str, pos))
                        {
                            var newStr = item.Path.AppendToNew(str[pos]);
                            newQueue.Add(new PathState() { State = trans.To, Path = newStr });
                            foreach (var emptyConnectedItem in GetStatesEmptyConnectedFrom(trans.To))
                            {
                                if (emptyConnectedItem == trans.To)
                                {
                                    continue;
                                }
                                newQueue.Add(new PathState() { State = emptyConnectedItem, Path = newStr });
                            }
                        }
                    }
                }
                queue = newQueue;
                ++pos;
            } while (pos <= str.Count);
            return new MatchResult() { LastMatchedString = lastMatchedString, Matched = false, EndState=lastState, EndGroupIdentity=lastStateGroup };
        }

        public override string ToString()
        {
            var stateStrs = "";
            for (var i = 0; i < States.Count; ++i)
            {
                if (i > 0)
                {
                    stateStrs += ", ";
                }
                stateStrs += States[i].ToString();
            }
            var acStateStrs = "";
            for (var i = 0; i < AcceptableStates.Count; ++i)
            {
                if (i > 0)
                {
                    acStateStrs += ", ";
                }
                acStateStrs += AcceptableStates[i].ToString();
            }
            var transStrs = "";
            for (var i = 0; i < Transforms.Count; ++i)
            {
                if (i > 0)
                {
                    transStrs += ", ";
                }
                transStrs += Transforms[i].ToString();
            }
            return string.Format("[NFAMachine: States={0}, Transforms={1}, StartState={2}, AcceptableStates={3}]", stateStrs, transStrs, StartState, acStateStrs);
        }
    }
}

