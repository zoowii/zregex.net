﻿using System;
using ZRegex.Net;

namespace ZRegex.Net
{
    // epsilon
    public class EmptyTransform : Transform
    {
        public EmptyTransform(State from, State to)
            : base(from, to)
        {

        }
        public override string ToString()
        {
            return string.Format("{0}-ε->{1}", From, To);
        }
        public override bool Match(RegexCharString str, int pos)
        {
            return false;
        }
        public override Transform CopyNew()
        {
            return new EmptyTransform(From, To);
        }
    }
    // char
    public class CharTransform : Transform
    {
        public RegexChar Char { get; set; }
        public CharTransform(State from, State to, RegexChar c)
            : base(from, to)
        {
            this.Char = c;
        }
        public override string ToString()
        {
            return string.Format("{0}-{1}->{2}", From, Char, To);
        }
        public override bool Match(RegexCharString str, int pos)
        {
            return str[pos].Equals(Char);
        }
        public override Transform CopyNew()
        {
            return new CharTransform(From, To, Char);
        }
    }
    public class CharNotInRangeTransform : Transform
    {
        public RegexCharString OutRange { get; set; }
        public CharNotInRangeTransform(State from, State to, RegexCharString outRange)
            :base(from, to)
        {
            this.OutRange = outRange;
        }
        public override string ToString()
        {
            return string.Format("{0}-not-{1}->{2}", From, OutRange, To);
        }
        public override bool Match(RegexCharString str, int pos)
        {
            return !OutRange.Contains(str[pos]);
        }
        public override Transform CopyNew()
        {
            return new CharNotInRangeTransform(From, To, OutRange);
        }
    }

    public class AnyCharTransform : Transform
    {
        public AnyCharTransform(State from, State to) : base(from, to) { }
        public override string ToString()
        {
            return string.Format("{0}-any-{1}", From, To);
        }
        public override bool Match(RegexCharString str, int pos)
        {
            return true;
        }
        public override Transform CopyNew()
        {
            return new AnyCharTransform(From, To);
        }
    }

    public class CharRangeTransform : Transform
    {
        public RegexChar StartChar { get; set; }
        public RegexChar EndChar { get; set; }
        public CharRangeTransform(State from, State to, RegexChar startChar, RegexChar endChar)
            : base(from, to)
        {
            this.StartChar = startChar;
            this.EndChar = endChar;
        }
        public override string ToString()
        {
            return string.Format("{0}-[{1},{2}]->{3}", From, StartChar, EndChar, To);
        }
        public override Transform CopyNew()
        {
            return new CharRangeTransform(From, To, StartChar, EndChar);
        }
        public override bool Match(RegexCharString str, int pos)
        {
            var c = (str[pos] as CharRegexChar).Char;
            var start = (StartChar as CharRegexChar).Char;
            var end = (EndChar as CharRegexChar).Char;
            return c >= start && c <= end;
        }
    }

    public class DigitTransform : CharRangeTransform
    {
        public DigitTransform(State from, State to) : base(from, to, new CharRegexChar('0'), new CharRegexChar('9')) { }
        public override Transform CopyNew()
        {
            return new DigitTransform(From, To);
        }
    }

    public class BigAlphaTransform : CharRangeTransform
    {
        public BigAlphaTransform(State from, State to)
            : base(from, to, new CharRegexChar('A'), new CharRegexChar('Z'))
        {

        }
        public override Transform CopyNew()
        {
            return new BigAlphaTransform(From, To);
        }
    }
    public class LittleAlphaTransform : CharRangeTransform
    {
        public LittleAlphaTransform(State from, State to)
            : base(from, to, new CharRegexChar('a'), new CharRegexChar('z'))
        {

        }
        public override Transform CopyNew()
        {
            return new LittleAlphaTransform(From, To);
        }
    }
}

