﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ZRegex.Net
{
    public class RegexExpr : NFAMachine
    {
        public RegexExpr()
            : base()
        {
        }
        public virtual RegexExpr CreateInstance()
        {
            return new RegexExpr();
        }
        public override RegexCharString CreateEmptyCharString()
        {
            return new StringRegexCharString("");
        }
        private State GetOrCreateInStateMapping(Dictionary<State, State> mapping, State state)
        {
            if (mapping.ContainsKey(state))
            {
                return mapping[state];
            }
            var newState = new State();
            newState.GroupIdentity = state.GroupIdentity;
            mapping[state] = newState;
            return newState;
        }
        /**
         * When copy, copy the states and there transforms
         */
        public RegexExpr CopyNew()
        {
            var expr = CreateInstance();
            var stateMapping = new Dictionary<State, State>();
            expr.StartState = GetOrCreateInStateMapping(stateMapping, this.StartState);
            expr.States.Clear();
            foreach (var state in States)
            {
                expr.States.Add(GetOrCreateInStateMapping(stateMapping, state));
            }
            expr.AcceptableStates.Clear();
            foreach (var state in AcceptableStates)
            {
                expr.AcceptableStates.Add(GetOrCreateInStateMapping(stateMapping, state));
            }
            expr.Transforms.Clear();
            foreach (var trans in Transforms)
            {
                var from = GetOrCreateInStateMapping(stateMapping, trans.From);
                var to = GetOrCreateInStateMapping(stateMapping, trans.To);
                var newTrans = trans.CopyNew();
                newTrans.From = from;
                newTrans.To = to;
                expr.Transforms.Add(newTrans);
            }
            return expr;
        }
        public void AddStates(RegexExpr other)
        {

            foreach (var state in other.States)
            {
                this.States.Add(state);
            }
        }
        public void AddTransforms(RegexExpr other)
        {
            foreach (var trans in other.Transforms)
            {
                this.Transforms.Add(trans);
            }
        }

        /**
         * A*B
         */
        public RegexExpr Concat(RegexExpr other)
        {
            var expr = new RegexExpr();
            var startState = new State();
            var endState = new State();
            startState.GroupIdentity = this.StartState.GroupIdentity;
            endState.GroupIdentity = this.StartState.GroupIdentity;
            var copiedThis = this.CopyNew();
            var copiedOther = other.CopyNew();
            expr.StartState = startState;
            expr.AcceptableStates.Add(endState);
            expr.States.Add(startState);
            expr.States.Add(endState);
            expr.AddStates(copiedThis);
            expr.AddStates(copiedOther);
            expr.AddTransforms(copiedThis);
            expr.AddTransforms(copiedOther);
            var startToAStart = new EmptyTransform(startState, copiedThis.StartState);
            expr.Transforms.Add(startToAStart);
            foreach (var state in copiedThis.AcceptableStates)
            {
                var trans = new EmptyTransform(state, copiedOther.StartState);
                expr.Transforms.Add(trans);
            }
            foreach (var state in copiedOther.AcceptableStates)
            {
                var trans = new EmptyTransform(state, endState);
                expr.Transforms.Add(trans);
            }
            return expr;
        }

        public static RegexExpr UnionAll(params RegexExpr[] exprs)
        {
            if (exprs.Length < 1)
            {
                throw new Exception("need at least one expr to union");
            }
            var pos = exprs.Length - 1;
            var result = exprs[pos];
            while (pos > 0)
            {
                pos -= 1;
                result = exprs[pos].Union(result);
            }
            return result;
        }

        /**
         * A|B
         */
        public RegexExpr Union(RegexExpr other)
        {
            var expr = new RegexExpr();
            var startState = new State();
            var endState = new State();
            var copiedThis = this.CopyNew();
            var copiedOther = other.CopyNew();
            expr.StartState = startState;
            expr.AcceptableStates.Add(endState);
            expr.States.Add(startState);
            expr.States.Add(endState);
            expr.AddStates(copiedThis);
            expr.AddStates(copiedOther);
            expr.AddTransforms(copiedThis);
            expr.AddTransforms(copiedOther);
            var startToAStart = new EmptyTransform(startState, copiedThis.StartState);
            var startToBStart = new EmptyTransform(startState, copiedOther.StartState);
            expr.Transforms.Add(startToAStart);
            expr.Transforms.Add(startToBStart);
            foreach (var state in copiedThis.AcceptableStates)
            {
                var trans = new EmptyTransform(state, endState);
                expr.Transforms.Add(trans);
            }
            foreach (var state in copiedOther.AcceptableStates)
            {
                var trans = new EmptyTransform(state, endState);
                expr.Transforms.Add(trans);
            }
            return expr;
        }

        /**
         * repeat at least zero times
         */
        public RegexExpr Closure()
        {
            var expr = new RegexExpr();
            var startState = new State();
            var endState = new State();
            startState.GroupIdentity = this.StartState.GroupIdentity;
            endState.GroupIdentity = this.StartState.GroupIdentity;
            expr.StartState = startState;
            expr.AcceptableStates.Add(endState);
            expr.States.Add(startState);
            expr.States.Add(endState);
            expr.AddStates(this);
            expr.AddTransforms(this);
            var startToOldStart = new EmptyTransform(startState, this.StartState);
            expr.Transforms.Add(startToOldStart);
            var startToEnd = new EmptyTransform(startState, endState);
            expr.Transforms.Add(startToEnd);
            foreach (var state in this.AcceptableStates)
            {
                var oldEndToEnd = new EmptyTransform(state, endState);
                expr.Transforms.Add(oldEndToEnd);
                var oldEndToOldStart = new EmptyTransform(state, this.StartState);
                expr.Transforms.Add(oldEndToOldStart);
            }
            return expr;
        }

        /**
         * one or more times
         */
        public RegexExpr Plus()
        {
            return this.CopyNew().Concat(this.Closure());
        }

        /**
         * repeat count times
         */
        public RegexExpr Repeat(int count)
        {
            RegexExpr result = new EmptyExpr();
            for (var i = 0; i < count; ++i)
            {
                result = result.Concat(this.CopyNew());
            }
            return result;
        }

        /**
         * repeat at least start times
         */
        public RegexExpr RepeatAtLeast(int min)
        {
            return Repeat(min).Concat(this.Closure());
        }

        /**
         * repeat [min, max] times
         */
        public RegexExpr Repeat(int min, int max)
        {
            if (max < min)
            {
                return new EmptyExpr();
            }
            if (max == min)
            {
                return Repeat(min);
            }
            var result = Repeat(min);
            for (var i = 0; i < max - min; ++i)
            {
                result = result.Concat(this.Optional());
            }
            return result;
        }

        /**
         * the regex expr is optional
         */
        public RegexExpr Optional()
        {
            return this.CopyNew().Union(new EmptyExpr());
        }

        public MatchResult Match(string str)
        {
            return Match(new StringRegexCharString(str));
        }
        public MatchResults MatchAll(string str)
        {
            return MatchAll(new StringRegexCharString(str));
        }
    }
}

