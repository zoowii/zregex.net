﻿using System;

namespace ZRegex.Net
{
	public class State
	{
		private static int _count = 0;
		public int Id{ get; set; }
        public string GroupIdentity { get; set; }
		public State ()
		{
			this.Id = ++_count;
		}
		public override string ToString ()
		{
			return string.Format ("{0}", Id);
		}
	}
}

