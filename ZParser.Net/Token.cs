﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZParser.Net
{
	public class Token
	{
		public IVar TokenType {get;set;}
		public string Text { get; set;}
		public Token ()
		{
		}
        public Token(string text, IVar type)
        {
            this.Text = text;
            this.TokenType = type;
        }
        public override string ToString()
        {
            return Text;
        }
        public static Token Empty = new Token("", EmptyVar.Instance);
	}
	public class TokenList : CList<Token>
	{
        public TokenList TailTokens
        {
            get
            {
                return this.Tail as TokenList;
            }
        }
        public TokenList(Token head, TokenList tail)
        {
            this.Head = head;
            this.Tail = tail;
        }
        public TokenList AddTokenToHead(Token token)
        {
            return new TokenList(token, this);
        }
        public override CList<Token> CloneNew()
        {
            return new TokenList(Head, this.Tail != null ? this.Tail.CloneNew() as TokenList : null);
        }
        public override CList<Token> Cons(Token item)
        {
            return new TokenList(item, this);
        }
        public override CList<Token> Append(Token item)
        {
            var result = this.CloneNew() as TokenList;
            var lastNode = result.LastNode as TokenList;
            lastNode.Tail = new TokenList(item, null);
            return result;
        }
        public static TokenList Create(IList<Token> tokens)
        {
            if (tokens.Count < 1)
            {
                return null;
            }
            var pos = tokens.Count - 1;
            var result = new TokenList(tokens[pos], null);
            while (pos > 0)
            {
                pos -= 1;
                result = result.AddTokenToHead(tokens[pos]);
            }
            return result;
        }
        public static TokenList Create(params Token[] tokens)
        {
            if (tokens.Length < 1)
            {
                return null;
            }
            var pos = tokens.Length - 1;
            var result = new TokenList(tokens[pos], null);
            while (pos > 0)
            {
                pos -= 1;
                result = result.AddTokenToHead(tokens[pos]);
            }
            return result;
        }
	}
}

