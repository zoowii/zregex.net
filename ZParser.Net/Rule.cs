﻿using System;
using System.Collections;
using System.Collections.Generic;
using ZRegex.Net;
using System.Text;

namespace ZParser.Net
{
    public class MatchRule
    {
        public IList<IVar> Items { get; set; }
        public MatchRule()
        {
            this.Items = new List<IVar>();
        }
    }
    public class RuleItem
    {
        public string ExtraInfo { get; set; }
        public IList<IVar> Items { get; set; }
        public RuleItem()
        {
            this.Items = new List<IVar>();
        }
        public IList<IVar> SubItems(int start = 0, int end = 0)
        {
            if (end == 0)
            {
                end = this.Items.Count;
            }
            var result = new List<IVar>();
            for (var i = start; i < end; ++i)
            {
                if (i >= 0 && i < this.Items.Count)
                {
                    result.Add(this.Items[i]);
                }
            }
            return result;
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            for (var i = 0; i < Items.Count; ++i)
            {
                if (i > 0)
                {
                    builder.Append(" ");
                }
                builder.Append(Items[i].ToString());
            }
            return builder.ToString();
        }
        public static RuleItem Create(params IVar[] items)
        {
            var result = new RuleItem();
            foreach (var item in items)
            {
                result.Items.Add(item);
            }
            return result;
        }
        public RuleItem Extra(string info)
        {
            this.ExtraInfo = info;
            return this;
        }
    }
    public class Rule
    {
        /**
         * destination var
         */
        public Var DestVar { get; set; }
        public IList<RuleItem> Items { get; set; }
        public Rule()
        {
            this.Items = new List<RuleItem>();
        }

        /**
         * 分离出所有子产生式中的直接左递归和非直接左递归
         * 
         * @return pair-left是直接左递归的项,pair-right是非直接左递归的项
         */
        public Pair<IList<RuleItem>, IList<RuleItem>> SplitByLeftRecursive()
        {
            var pair = new Pair<IList<RuleItem>, IList<RuleItem>>();
            pair.Left = new List<RuleItem>();
            pair.Right = new List<RuleItem>();
            foreach (var item in this.Items)
            {
                if (item.Items.Count > 0 && item.Items[0] == this.DestVar)
                {
                    pair.Left.Add(item);
                }
                else
                {
                    pair.Right.Add(item);
                }
            }
            return pair;
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append(DestVar.ToString());
            builder.Append(" -> ");
            for (var i = 0; i < Items.Count; ++i)
            {
                if (i > 0)
                {
                    builder.Append(" | ");
                }
                builder.Append(Items[i].ToString());
            }
            return builder.ToString();
        }
    }
}

