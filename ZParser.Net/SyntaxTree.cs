﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZParser.Net
{
    public class SyntaxTreeNode
    {
        public string ExtraInfo { get; set; }
        public IVar NodeVar { get; set; }
        public Token ValueToken { get; set; }
        public IList<SyntaxTreeNode> Items { get; set; }
        public bool IsBound { get; set; }

        /**
         * 指向父节点，只在要用到时被设置
         */
        public SyntaxTreeNode Parent { get; set; }
        public SyntaxTreeNode()
        {
            this.Items = new List<SyntaxTreeNode>();
            this.IsBound = false;
        }
        public SyntaxTreeNode(IVar nodeVar)
        {
            this.Items = new List<SyntaxTreeNode>();
            this.NodeVar = nodeVar;
            this.IsBound = false;
        }
        public SyntaxTreeNode(IVar nodeVar, Token valueToken)
        {
            this.Items = new List<SyntaxTreeNode>();
            this.NodeVar = nodeVar;
            this.IsBound = false;
            this.ValueToken = valueToken;
        }
        public void MarkSubParents()
        {
            if (Items != null)
            {
                foreach (var item in Items)
                {
                    item.Parent = this;
                    item.MarkSubParents();
                }
            }
        }
        public SyntaxTreeNode CloneNew()
        {
            var node = new SyntaxTreeNode();
            node.NodeVar = this.NodeVar;
            node.ValueToken = this.ValueToken;
            foreach (var item in this.Items)
            {
                node.Items.Add(item.CloneNew());
            }
            node.IsBound = this.IsBound;
            return node;
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            if(NodeVar is FinalVar && IsBound)
            {
                return ValueToken.ToString();
            }
            bool showParent = this.Items.Count < 1;
            if (showParent)
            {
                builder.Append(string.Format("{0}", NodeVar));
            }
            if (ValueToken != null)
            {
                builder.Append(string.Format("{0}", ValueToken));
            }
            if (showParent)
            {
                if (Items.Count > 0)
                {
                    builder.Append("[");
                }
            }
            for (var i = 0; i < Items.Count; ++i)
            {
                if (i > 0)
                {
                    builder.Append(" ");
                }
                builder.Append(Items[i]);
            }
            if (showParent)
            {
                if (Items.Count > 0)
                {
                    builder.Append("]");
                }
            }
            return builder.ToString();
        }
        public int MinCount
        {
            get
            {
                if (NodeVar is FinalVar)
                {
                    return (NodeVar as FinalVar).MinCount;
                }
                else
                {
                    var minCount = 0;
                    foreach(var item in Items)
                    {
                        minCount += item.MinCount;
                    }
                    return minCount;
                }
            }
        }
    }
	public class SyntaxTree
	{
        public SyntaxTreeNode RootNode { get; set; }
		public SyntaxTree ()
		{
		}
        public SyntaxTreeNode LeftUnBoundVarOfNode(SyntaxTreeNode node)
        {
            if(!node.IsBound)
            {
                return node;
            }
            foreach (var item in node.Items)
            {
                var res = LeftUnBoundVarOfNode(item);
                if (res != null)
                {
                    return res;
                }
            }
            return null;
        }
        private SyntaxTreeNode FirstMiddleVarNodeUnder(SyntaxTreeNode node)
        {
            if (node.NodeVar.IsMiddleVar)
            {
                return node;
            }
            foreach (var item in node.Items)
            {
                var res = FirstMiddleVarNodeUnder(item);
                if (res != null)
                {
                    return res;
                }
            }
            return null;
        }
        /**
         * 找到第一个父节点只包含一个子节点，且父子节点的NodeVar是同一个Var，返回这种情况下的子节点
         */
        private SyntaxTreeNode FirstRepeatNodeUnder(SyntaxTreeNode node)
        {
            if (node.Items.Count == 1 && node.NodeVar == node.Items[0].NodeVar)
            {
                return node.Items[0];
            }
            foreach (var item in node.Items)
            {
                var res = FirstRepeatNodeUnder(item);
                if (res != null)
                {
                    return res;
                }
            }
            return null;
        }
        public SyntaxTreeNode FirstRepeatNode
        {
            get
            {
                return FirstRepeatNodeUnder(RootNode);
            }
        }
        public SyntaxTreeNode FirstMiddleVarNode
        {
            get
            {
                return FirstMiddleVarNodeUnder(RootNode);
            }
        }
        public SyntaxTreeNode LeftUnBoundVar
        {
            get
            {
                return LeftUnBoundVarOfNode(RootNode);
            }
        }
        public bool IsFinished
        {
            get
            {
                return LeftUnBoundVar == null;
            }
        }
        public SyntaxTree CloneNew()
        {
            var other = new SyntaxTree();
            other.RootNode = this.RootNode.CloneNew();
            return other;
        }
        public override string ToString()
        {
            return RootNode.ToString();
        }
        public int MinCount
        {
            get
            {
                return RootNode.MinCount;
            }
        }
	}
}

