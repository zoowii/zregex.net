﻿ZParser.Net
====
一个简单的语法分析引擎

## Features

* 简单的基于广度优先搜索的语法分析引擎

## TODO

* 增加一个使用友好的API(类似bison/yacc)
* 增加和ZRegex.Net的adpater，从而可以直接一起使用(做成parser combinator的形式)
* 可视化语法分析树
* 使用范式简化parser的语法规则列表，从而移除不可达分支，增加分支的命中率和减少匹配深度的层次
* 提供类似parser combinator的API，从而使得API使用非常方便
* 语法parser要能消除直接左递归和间接左递归(直接左递归的消除已实现,间接左递归目前还没用到,以后再写)
