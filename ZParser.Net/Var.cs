﻿using System;

namespace ZParser.Net
{
	public interface IVar
	{
		string Name { get;}
        bool IsMiddleVar { get; }
        bool IsNotLeftVar { get; }
        IVar OriginVar { get; }
        int MinCount {get;}
	}
	public class Var : IVar
	{
		public string Name { get; private set;}
        /**
         * 是否是程序中间自动生成的Var
         */
        public bool IsMiddleVar { get; set; }
        public bool IsNotLeftVar { get; set; }
        public IVar OriginVar { get; set; }
		public Var (string name)
		{
			this.Name = name;
            this.IsMiddleVar = false;
            this.IsNotLeftVar = false;
		}
        public override string ToString()
        {
            return Name;
        }
        public int MinCount {
            get
            {
                return 1;
            }
        }
	}
    public class FinalVar : IVar
	{
		public string Name {get; private set;}
        /**
         * 是否是程序中间自动生成的Var
         */
        public bool IsMiddleVar { get; set; }
        public bool IsNotLeftVar { get; set; }
        public IVar OriginVar { get; set; }
		public FinalVar(string name)
		{
			this.Name = name;
            this.IsMiddleVar = false;
            this.IsNotLeftVar = false;
		}
        public override string ToString()
        {
            return Name;
        }
        public virtual int MinCount {
            get
            {
                return 1;
            }
        }
	}
    public class EmptyVar : FinalVar
    {
        public EmptyVar(): base("")
        {
        }
        public override int MinCount
        {
            get
            {
                return 0;
            }
        }
        public static EmptyVar Instance = new EmptyVar();
    }
}

