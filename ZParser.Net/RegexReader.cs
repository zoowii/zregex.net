﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZRegex.Net;

namespace ZParser.Net
{
    /**
     * load RegexExpr from regex string
     */
    public class RegexReader
    {
        private static CharExpr C(char c)
        {
            return new CharExpr(c);
        }
        private static Dictionary<string, FinalVar> FinalVarDict { get; set; }
        public static FinalVar InternFinalVar(string name)
        {
            if (FinalVarDict == null)
            {
                FinalVarDict = new Dictionary<string, FinalVar>();
            }
            if (!FinalVarDict.ContainsKey(name))
            {
                FinalVarDict[name] = new FinalVar(name);
            }
            return FinalVarDict[name];
        }
        /**
         * Supported features
         * '|'
         * ( ... ) group
         * \d digit
         * \w alpha or digit
         * \uabcd unicode char support
         * a-b char range
         * [abc] union
         * abc concat
         * a+ repeat at least one times 
         * a* repeat at least zero times
         * a? repeat one or zero times
         * a{m[,n]} repeat at least m times [and at most n times]
         * . any char
         * \s space
         * \\ \+ \* \{ \[ \( \| \? \. \- ... escape
         */
        public static RegexExpr Read(string regex)
        {
            var escape = C('\\'); // \
            var escapeEscape = escape.Concat(escape); // \\
            escapeEscape.MarkGroup(@"\\");
            var escapeAdd = escape.Concat(C('+')); // \+
            escapeAdd.MarkGroup(@"\+");
            var escapeMul = escape.Concat(C('*')); // \*
            escapeMul.MarkGroup(@"\*");
            var escapeHkh = escape.Concat(C('{')); // \{
            escapeHkh.MarkGroup(@"\{");
            var escapeZkh = escape.Concat(C('[')); // \[
            escapeZkh.MarkGroup(@"\[");
            var escapeXkh = escape.Concat(C('(')); // \(
            escapeXkh.MarkGroup(@"\(");
            var escapeRightHkh = escape.Concat(C('}')); // \}
            escapeRightHkh.MarkGroup(@"\}");
            var escapeRightZkh = escape.Concat(C(']')); // \]
            escapeRightZkh.MarkGroup(@"\]");
            var escapeRightXkh = escape.Concat(C(')')); // \)
            escapeRightXkh.MarkGroup(@"\)");
            var escapeOr = escape.Concat(C('|')); // \|
            escapeOr.MarkGroup(@"\|");
            var escapeOptional = escape.Concat(C('?')); // \?
            escapeOptional.MarkGroup(@"\?");
            var escapeAny = escape.Concat(C('.')); // \.
            escapeAny.MarkGroup(@"\.");
            var escapeTo = escape.Concat(C('-')); // \-
            escapeTo.MarkGroup(@"\-");
            var space = escape.Concat(C('s')); // \s
            space.MarkGroup(@"\s");
            var digit = escape.Concat(C('d')); // \d
            digit.MarkGroup(@"\d");
            var alphaOrDigit = escape.Concat(C('w')); // \w
            alphaOrDigit.MarkGroup(@"\w");
            var unicode = escape.Concat(C('u')).Concat(new DigitExpr().Union(new AlphaExpr()).Repeat(4));
            unicode.MarkGroup(@"unicode");
            var add = C('+');
            add.MarkGroup(@"+");
            var closure = C('*');
            closure.MarkGroup(@"*");
            var optional = C('?');
            optional.MarkGroup(@"?");
            var any = C('.');
            any.MarkGroup(@".");
            var leftXkh = C('(');
            leftXkh.MarkGroup(@"(");
            var rightXkh = C(')');
            rightXkh.MarkGroup(@")");
            var leftZkh = C('[');
            leftZkh.MarkGroup(@"[");
            var rightZkh = C(']');
            rightZkh.MarkGroup(@"]");
            var leftDkh = C('{');
            leftDkh.MarkGroup(@"{");
            var rightDkh = C('}');
            rightDkh.MarkGroup(@"}");
            var comma = C(',');
            comma.MarkGroup(@",");
            var or = C('|');
            or.MarkGroup(@"|");
            var range = C('-');
            range.MarkGroup(@"-");
            // var charExpr = new CharRangeExpr((char)0, (char)127);
            var charExpr = new CharNotInRangeExpr(@"+*?.()[]{},-|");
            charExpr.MarkGroup(@"char");
            var regexReaderExpr = RegexExpr.UnionAll(escapeEscape, escapeAdd, escapeMul, escapeHkh, escapeZkh,
                escapeXkh, escapeRightHkh, escapeRightZkh, escapeRightXkh, escapeOr, escapeOptional, escapeAny, escapeTo, space, digit, alphaOrDigit, unicode, add, closure, optional, any, leftXkh, rightXkh,
                leftZkh, rightZkh, leftDkh, rightDkh, comma, or, range, charExpr);
            regexReaderExpr.Build();
            var regexMatchedTokensFromRegex = regexReaderExpr.MatchAll(regex);
            var tokenListBuiding = new List<Token>();
            foreach (var tokenFromRegex in regexMatchedTokensFromRegex.MatchedItems)
            {
                var token = new Token() { Text = tokenFromRegex.MatchedString.ToString(), TokenType = InternFinalVar(tokenFromRegex.GroupIdentity) };
                tokenListBuiding.Add(token);
            }
            var tokens = TokenList.Create(tokenListBuiding);
            var EVar = new Var("E");
            var IVar = new Var("I");
            var cSeqVar = new Var("CharSeq");
            var iRule = new Rule()
            {
                DestVar = IVar,
                Items =
                {
                    RuleItem.Create(InternFinalVar(escapeEscape.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeTo.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeAdd.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeMul.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeHkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeRightHkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeZkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeRightZkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeXkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeRightXkh.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeOr.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeOptional.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(escapeAny.GroupIdentity)),
                    RuleItem.Create(InternFinalVar(space.GroupIdentity)).Extra("space"),
                    RuleItem.Create(InternFinalVar(digit.GroupIdentity)).Extra("digit"),
                    RuleItem.Create(InternFinalVar(alphaOrDigit.GroupIdentity)).Extra("alphaOrDigit"),
                    RuleItem.Create(InternFinalVar(unicode.GroupIdentity)).Extra("unicode"),
                    RuleItem.Create(InternFinalVar(charExpr.GroupIdentity))
                }
            };
            var cSeqRule = new Rule()
            {
                DestVar = cSeqVar,
                Items =
                {
                    RuleItem.Create(IVar),
                    RuleItem.Create(IVar, InternFinalVar(range.GroupIdentity), IVar).Extra("a-b"),
                    RuleItem.Create(IVar, cSeqVar).Extra("CharSeqConcat")
                }
            };
            var eRule = new Rule()
            {
                DestVar = EVar,
                Items =
                {
                    RuleItem.Create(InternFinalVar(leftXkh.GroupIdentity), EVar, InternFinalVar(rightXkh.GroupIdentity)).Extra("(a)"),
                    RuleItem.Create(EVar, InternFinalVar(or.GroupIdentity), EVar).Extra("a|b"),
                    RuleItem.Create(IVar),
                    RuleItem.Create(cSeqVar),
                    RuleItem.Create(InternFinalVar(leftZkh.GroupIdentity), cSeqVar, InternFinalVar(rightZkh.GroupIdentity)).Extra("[abc]"),
                    // new RuleItem(){Items={
                       //                InternFinalVar(leftZkh.GroupIdentity), IVar, InternFinalVar(range.GroupIdentity), IVar, InternFinalVar(rightZkh.GroupIdentity)
                         //          }}.Extra("[a-b]"),
                    RuleItem.Create(EVar, InternFinalVar(closure.GroupIdentity)).Extra("Closure"),
                    RuleItem.Create(EVar, InternFinalVar(add.GroupIdentity)).Extra("Repeat+"),
                    RuleItem.Create(EVar, InternFinalVar(optional.GroupIdentity)).Extra("Optional"),
                    RuleItem.Create(EVar, InternFinalVar(leftDkh.GroupIdentity), cSeqVar, InternFinalVar(rightDkh.GroupIdentity)).Extra("x{m}"),
                    RuleItem.Create(EVar,  InternFinalVar(leftDkh.GroupIdentity), cSeqVar, InternFinalVar(comma.GroupIdentity), cSeqVar,
                                       InternFinalVar(rightDkh.GroupIdentity)).Extra("x{m,n}"),
                    RuleItem.Create(EVar, EVar).Extra("Concat")
                }
            };
            var regexParser = new Parser() { StartVar = EVar, Rules = { iRule, cSeqRule, eRule }, Vars = { EVar, IVar, cSeqVar } };
            var regexSyntaxTree = regexParser.Parse(tokens);
            return ParseRegexStringSyntaxTreeNodeToRegexExpr(regexSyntaxTree.RootNode);
        }


        /**
         * 获得IVar节点的文本内容，如果有转义，要去掉转义符
         */
        private static string GetTextOfIVarNode(SyntaxTreeNode node)
        {
            return node.Items[0].Items[0].ValueToken.Text; // FIXME
        }

        /**
         * 因为cSeqVar节点是类似head-tail的结构，所以用这个方法来获取其实实际的所有元素
         */
        private static IList<SyntaxTreeNode> GetAllItemsInCharSeqNode(SyntaxTreeNode node)
        {
            if(node.NodeVar.Name != "CharSeq" || node.Items.Count>2) {
                return new List<SyntaxTreeNode>(){node};
            }
            if(node.Items.Count==1 || node.Items.Count==2)
            {
                var result = new List<SyntaxTreeNode>();
                foreach(var item in node.Items){
                    result.AddRange(GetAllItemsInCharSeqNode(item));
                }
                return result;
            }
            throw new Exception("impossible state");
        }

        /**
         * 从cSeqVar的节点中找到所有的字符
         * TODO: 这里还没考虑\w这类字符
         */
        private static string GetAllCharsInCharSeqNode(SyntaxTreeNode node, string initial = "")
        {
            if (node.Items.Count < 1)
            {
                return initial;
            }
            if (node.Items.Count == 1)
            {
                return GetTextOfIVarNode(node) + initial;
            }
            if (node.Items.Count == 2)
            {
                return node.Items[0].Items[0].ValueToken.Text[0] + GetAllCharsInCharSeqNode(node.Items[1], initial);
            }
            throw new Exception("not supported get chars in char seq node");
        }

        private static RegexExpr ParseRegexStringSyntaxTreeNodeToRegexExpr(SyntaxTreeNode node)
        {
            if (node.NodeVar.Name=="char")
            {
                return new CharExpr(node.ValueToken.Text[0]);
            }
            if (node.Items.Count == 1)
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]);
            }
            if (node.NodeVar.Name == "I")
            {
                if (node.Items[0].ValueToken.Text.Length > 1 && node.Items[0].ValueToken.Text.StartsWith(@"\"))
                {
                    return new CharExpr(node.Items[0].ValueToken.Text[1]);
                }
                if (node.ExtraInfo == "space")
                {
                    return CharExpr.CreateUnionFromChars(" \n\t\r");
                }
                if (node.ExtraInfo == "digit")
                {
                    return new DigitExpr();
                }
                if (node.ExtraInfo == "alphaOrDigit")
                {
                    return new AlphaExpr().Union(new DigitExpr());
                }
                if (node.ExtraInfo == "unicode")
                {
                    return new CharExpr('\0'); // FIXME: now unicode not supported
                }
                throw new Exception("not supported char type now");
            }
            if (node.ExtraInfo == "(a)")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[1]);
            }
            if (node.ExtraInfo == "a|b")
            {
                return RegexExpr.UnionAll(ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]), ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[2]));
            }
            if (node.ExtraInfo == "Closure")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Closure();
            }
            if (node.ExtraInfo == "Repeat+")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Plus();
            }
            if (node.ExtraInfo == "Optional")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Optional();
            }
            if (node.ExtraInfo == "x{m}")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Repeat(Int32.Parse(GetAllCharsInCharSeqNode(node.Items[2])));
            }
            if (node.ExtraInfo == "x{m,n}")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Repeat(Int32.Parse(node.Items[2].ValueToken.Text), Int32.Parse(node.Items[4].ValueToken.Text));
            }
            if (node.ExtraInfo == "Concat")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Concat(ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[1]));
            }
            if (node.ExtraInfo == "a-b")
            {
                return new CharRangeExpr(node.Items[0].ValueToken.Text[0], node.Items[2].ValueToken.Text[1]);
            }
            if (node.ExtraInfo == "CharSeqConcat")
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]).Concat(ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[1]));
            }
            if (node.ExtraInfo == "[abc]")
            {
                var items = GetAllItemsInCharSeqNode(node.Items[1]);
                var exprItems = new RegexExpr[items.Count];
                for (var i = 0; i < items.Count; ++i)
                {
                    exprItems[i] = ParseRegexStringSyntaxTreeNodeToRegexExpr(items[i]);
                }
                return RegexExpr.UnionAll(exprItems.ToArray());
            }
            if (node.Items.Count == 1)
            {
                return ParseRegexStringSyntaxTreeNodeToRegexExpr(node.Items[0]);
            }
            if (node.NodeVar.Name == @"\s")
            {
                return CharExpr.CreateUnionFromChars(" \n\t\r");
            }
            if (node.NodeVar.Name == @"\.")
            {
                return new AnyCharExpr();
            }
            if (node.NodeVar.Name == @"\d")
            {
                return new DigitExpr();
            }
            if (node.NodeVar.Name == @"\w")
            {
                return new AlphaExpr().Union(new DigitExpr());
            }
            if (new List<string>() {@"\\", @"\+", @"\-", @"\(", @"\)", @"\[", @"\]", @"\{", @"\}", @"\*", @"\,", @"\?"}.Contains(node.NodeVar.Name))
            {
                return new CharExpr(node.NodeVar.Name[1]);
            }
            if (node.NodeVar.Name == "unicode")
            {
                return new CharExpr('\0'); // FIXME: unicode not supported now
            }
            return null;
        }
    }
}
