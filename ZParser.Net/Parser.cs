﻿using System;
using System.Collections.Generic;
using System.Collections;
using ZRegex.Net;

namespace ZParser.Net
{
    public class MatchOption
    {
        public SyntaxTree Tree { get; set; }
        public TokenList RemainingTokens { get; set; }
        public int RemainingTokensCount
        {
            get
            {
                return RemainingTokens != null ? RemainingTokens.Count : 0;
            }
        }
        public MatchOption(SyntaxTree tree, TokenList remainingTokens)
        {
            this.Tree = tree;
            this.RemainingTokens = remainingTokens;
        }
        public bool IsFinshed
        {
            get
            {
                return Tree.IsFinished && RemainingTokensCount < 1;
            }
        }
        public bool IsEnd
        {
            get
            {
                return RemainingTokensCount < 1;
            }
        }
        public override string ToString()
        {
            return Tree.ToString() + (RemainingTokens != null ? RemainingTokens.ToString() : "");
        }
    }
    public class Parser
    {
        /**
         * initial var
         */
        public Var StartVar { get; set; }
        public IList<Rule> Rules { get; set; }
        private IList<Rule> OriginRules { get; set; }
        private IDictionary<Var, Rule> DestRuleMapping { get; set; }
        public IList<IVar> Vars { get; set; }

        public Parser()
        {
            this.Rules = new List<Rule>();
            this.Vars = new List<IVar>();
        }

        /**
         * 按DestVar对rules进行分组
         */
        private void GroupRulesByDestVar()
        {
            var destVars = new HashSet<Var>();
            foreach (var rule in Rules)
            {
                destVars.Add(rule.DestVar);
            }
            var groupedRules = new List<Rule>();
            foreach (var destVar in destVars)
            {
                var rules = FindRulesOfDestVar(destVar);
                if (rules != null)
                {
                    var groupRule = new Rule();
                    groupRule.DestVar = destVar;
                    foreach (var rule in rules)
                    {
                        foreach (var item in rule.Items)
                        {
                            groupRule.Items.Add(item);
                        }
                    }
                    groupedRules.Add(groupRule);
                }
            }
            this.Rules = groupedRules;
        }

        private void BuildDestRuleMapping()
        {
            this.DestRuleMapping = new Dictionary<Var, Rule>();
            foreach (var rule in this.Rules)
            {
                this.DestRuleMapping[rule.DestVar] = rule;
            }
        }

        private static long _incNo = 1L;

        private static long NextIncNo()
        {
            return _incNo++;
        }

        private bool _initialized = false;

        /*
         * 需要消除直接左递归和间接左递归（间接左递归可以转成直接左递归再消除）
         * 直接左递归的消除方法：
         * 按DestVar对所有Rule产生式进行分组，
         * 然后对于每一组，根据第一个子产生式的第一项是否是DestVar本身来判断是否是直接左递归
         * 是直接左递归的那一组，都是以DestVar开头，所以可以组合成一个DestVar'的新Var，内容是上述直接左递归的子产生式的除了第一项外的内容，然后DestVar=>DestVar + DestVar'
         * 另外非直接左递归的那一组可以组成一个新的DestVar''的新Var，所以DestVar => DestVar''
         * 然后DestVar => DestVar + DestVar' | DestVar''
         * 然后就可以改写成 DestVar => DestVar'' + P' 和P' => DestVar' + P' | ε
         * 这样直接左递归就消除了
         * 
         * TODO: 暂时没考虑间接左递归
         * TODO: 移除无用规则
         * TODO: 对于A=>B唯一的情况,可以直接A=> B的所有子产生式
         * TODO: 其他优化
         */
        public Parser Build()
        {
            if (_initialized)
            {
                return this;
            }
            this._initialized = true;
            this.OriginRules = this.Rules;
            this.GroupRulesByDestVar();
            var finalRules = new List<Rule>();
            foreach (var rule in this.Rules)
            {
                var splitedByLeftRecPair = rule.SplitByLeftRecursive();
                var leftRecItems = splitedByLeftRecPair.Left;
                var notLeftRecItems = splitedByLeftRecPair.Right;
                if (leftRecItems.Count > 0 && notLeftRecItems.Count < 1)
                {
                    throw new Exception("the parse rule can't be used " + rule);
                }
                if (leftRecItems.Count < 1)
                {
                    finalRules.Add(rule);
                    continue;
                }
                Var leftRecItemVar; // 以上算法描述中的DestVar => DestVar + DestVar' 这个子产生式中的DestVar'
                Var notLeftRecItemVar; // 以上算法描述中的DestVar => DestVar'' 这个子产生式中的DestVar''
                // 产生一个新Var代表上述直接左递归子产生式中的去掉头部的部分
                var DestVarLeftRecReplaceVar = new Var(rule.DestVar.Name + "@@left-" + NextIncNo()) { IsMiddleVar = true, OriginVar = rule.DestVar };
                var DestVarLeftRecReplaceVarRule = new Rule() { DestVar = DestVarLeftRecReplaceVar };
                foreach (var item in leftRecItems)
                {
                    if (item.Items.Count > 0)
                    {
                        var subItem = new RuleItem() { Items = item.SubItems(1), ExtraInfo = item.ExtraInfo };
                        DestVarLeftRecReplaceVarRule.Items.Add(subItem);
                    }
                }
                finalRules.Add(DestVarLeftRecReplaceVarRule);
                this.Vars.Add(DestVarLeftRecReplaceVar);
                leftRecItemVar = DestVarLeftRecReplaceVar;
                // 产生一个新Var代表上述非直接左递归子产生式中的全部
                var DestVarNotLeftRecReplaceVar = new Var(rule.DestVar.Name + "@@not-left-" + NextIncNo()) { IsMiddleVar = true, IsNotLeftVar = true, OriginVar = rule.DestVar };
                var DestVarNotLeftRecReplaceVarRule = new Rule() { DestVar = DestVarNotLeftRecReplaceVar, Items = notLeftRecItems };
                finalRules.Add(DestVarNotLeftRecReplaceVarRule);
                this.Vars.Add(DestVarNotLeftRecReplaceVar);
                notLeftRecItemVar = DestVarNotLeftRecReplaceVar;
                var pVar = new Var(rule.DestVar.Name + "@@p-" + NextIncNo()) { IsMiddleVar = true, OriginVar = rule.DestVar }; // 上面算法描述中的P'
                this.Vars.Add(pVar);
                var pVarRule = new Rule()
                {
                    DestVar = pVar,
                    Items =
                    {
                        new RuleItem(){Items={leftRecItemVar, pVar}},
                        new RuleItem(){Items={new EmptyVar(){IsMiddleVar=true,OriginVar=rule.DestVar}}}
                    }
                };
                finalRules.Add(pVarRule);
                var destVarRule = new Rule()
                {
                    DestVar = rule.DestVar,
                    Items =
                    {
                        new RuleItem(){Items={notLeftRecItemVar, pVar}}
                    }
                };
                finalRules.Add(destVarRule);
            }
            this.Rules = finalRules;
            this.BuildDestRuleMapping();
            return this;
        }
        private IList<Rule> FindRulesOfDestVar(IVar destVar, IList<Rule> rules = null)
        {
            var usingRules = rules != null ? rules : this.Rules;
            if (DestRuleMapping == null || rules != null)
            {
                var result = new List<Rule>();
                foreach (var rule in usingRules)
                {
                    if (rule.DestVar == destVar)
                    {
                        result.Add(rule);
                    }
                }
                return result;
            }
            else
            {
                if (destVar is Var && DestRuleMapping.ContainsKey(destVar as Var))
                {
                    return new List<Rule>() { DestRuleMapping[destVar as Var] };
                }
                else
                {
                    return new List<Rule>();
                }
            }
        }
        public SyntaxTree Parse(TokenList tokens)
        {
            this.Build();
            var tree = new SyntaxTree() { RootNode = new SyntaxTreeNode(StartVar) };
            var allOptions = new List<MatchOption>();
            allOptions.Add(new MatchOption(tree, tokens));
            var tokensCount = tokens.Count;
            while (allOptions.Count > 0)
            {
                var option = allOptions[0];
                allOptions.RemoveAt(0);
                if (option.IsFinshed)
                {
                    var finalTree = option.Tree;
                    finalTree = GetSyntaxTreeFromOriginRules(finalTree);
                    MarkExtraInfoInSyntaxTreeNode(finalTree.RootNode, this.OriginRules);
                    return finalTree;
                }
                if (option.Tree.MinCount > tokensCount)
                {
                    continue;
                }
                var options = TryMatchNext(option.Tree, option.RemainingTokens);
                if (options != null)
                {
                    foreach (var item in options)
                    {
                        allOptions.Add(item);
                    }
                }
            }
            return null;
        }

        /**
         * 自底向上把NodeVar的Name包含@@的节点上钻与最初定义的文法规则进行匹配，从而生成直接满足最初的文法规则的抽象语法树，方便下一步的使用
         * 从抽象语法树的最左下角开始寻找（应该不一定要从左下角开始，但是选择这样统一逻辑），不断把中间过程的Var（只是left-var和p-var）的所有Items直接替换掉原来此Var的位置,如果此中间Var是not-left-var，则不这样做，而是把此not-left-var替换成原始的Var
         * 碰到 EVar=>EVar这种的，直接简化成一层，碰到EmptyVar(IsMiddleVar=true)的，直接忽略掉
         */
        private SyntaxTree GetSyntaxTreeFromOriginRules(SyntaxTree tree)
        {
            tree.RootNode.MarkSubParents();
            var curNode = tree.FirstMiddleVarNode;
            while (curNode != null)
            {
                if (curNode.Parent == null)
                {
                    throw new Exception("inpossible state in common logic");
                }
                if (curNode.NodeVar is EmptyVar)
                {
                    curNode.Parent.Items.Remove(curNode);
                }
                else if (curNode.NodeVar.IsNotLeftVar)
                {
                    curNode.NodeVar = curNode.NodeVar.OriginVar;
                }
                else
                {
                    var items = curNode.Items;
                    var idx = curNode.Parent.Items.IndexOf(curNode);
                    curNode.Parent.Items.RemoveAt(idx);
                    for (var i = items.Count - 1; i >= 0; --i)
                    {
                        var item = items[i];
                        item.Parent = curNode.Parent;
                        curNode.Parent.Items.Insert(idx, item);
                    }
                }
                curNode = tree.FirstMiddleVarNode;
            }
            // 简化EVar=>EVar这种为一层
            curNode = tree.FirstRepeatNode;
            while (curNode != null)
            {
                if (curNode.Parent == null)
                {
                    throw new Exception("inpossible state in common logic");
                }
                var items = curNode.Items;
                foreach (var item in items)
                {
                    item.Parent = curNode.Parent;
                }
                curNode.Parent.Items = curNode.Items;
                curNode = tree.FirstRepeatNode;
            }
            return tree;
        }

        /**
         * 在已经parser成功的语法树上,重新从根节点开始进行匹配rules,并且在上面标注RuleItem的ExtraInfo
         */
        private void MarkExtraInfoInSyntaxTreeNode(SyntaxTreeNode node, IList<Rule> rules)
        {
            var foundRules = FindRulesOfDestVar(node.NodeVar, rules);
            if (foundRules == null || foundRules.Count < 1)
            {
                return;
            }
            var rule = foundRules[0];
            foreach (var ruleItem in rule.Items)
            {
                if (node.Items.Count != ruleItem.Items.Count)
                {
                    continue;
                }
                var matched = true;
                for (var i = 0; i < ruleItem.Items.Count; ++i)
                {
                    if (node.Items[i].NodeVar != ruleItem.Items[i])
                    {
                        matched = false;
                        break;
                    }
                }
                if (matched)
                {
                    node.ExtraInfo = ruleItem.ExtraInfo;
                    foreach (var item in node.Items)
                    {
                        MarkExtraInfoInSyntaxTreeNode(item, rules);
                    }
                    if (ruleItem.ExtraInfo != null && ruleItem.ExtraInfo.Length > 0)
                    {
                        node.ExtraInfo = ruleItem.ExtraInfo;
                    }
                    else
                    {
                        foreach (var item in node.Items)
                        {
                            if (item.ExtraInfo != null && item.ExtraInfo.Length > 0)
                            {
                                node.ExtraInfo = item.ExtraInfo;
                                break;
                            }
                        }
                    }
                    return;
                }
            }
        }
        /**
         * when match next rule, may produce multiple options
         */
        protected IList<MatchOption> TryMatchNext(SyntaxTree tree, TokenList tokens, bool recursive = true)
        {
            var options = new List<MatchOption>();
            var remainingTokens = tokens;
            var remainingTokensCount = remainingTokens != null ? remainingTokens.Count : 0;
            if (remainingTokensCount > 0 || tree.LeftUnBoundVar != null)
            {
                tree = tree.CloneNew();
                var leftNode = tree.LeftUnBoundVar;
                if (leftNode == null)
                {
                    if (tokens.Count > 0)
                    {
                        return null;
                    }
                    else
                    {
                        options.Add(new MatchOption(tree, tokens));
                        return options;
                    }
                }
                else if (leftNode.NodeVar is FinalVar)
                {
                    if (leftNode.NodeVar is EmptyVar)
                    {
                        leftNode.ValueToken = Token.Empty;
                        leftNode.IsBound = true;
                        options.Add(new MatchOption(tree, tokens));
                        return options;
                    }
                    else if (tokens.Head.TokenType == leftNode.NodeVar)
                    {
                        leftNode.ValueToken = tokens.Head;
                        leftNode.IsBound = true;
                        options.Add(new MatchOption(tree, tokens.TailTokens));
                        return options;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    var rules = FindRulesOfDestVar(leftNode.NodeVar);
                    foreach (var rule in rules)
                    {
                        foreach (var ruleItem in rule.Items)
                        {
                            var optionTree = tree.CloneNew();
                            var optionLeftNode = optionTree.LeftUnBoundVar;
                            foreach (var ruleItemVar in ruleItem.Items)
                            {
                                optionLeftNode.IsBound = true;
                                optionLeftNode.Items.Add(new SyntaxTreeNode(ruleItemVar));
                            }
                            options.Add(new MatchOption(optionTree, tokens));
                        }
                    }
                    return options;
                }
            }
            else
            {
                options.Add(new MatchOption(tree, tokens));
                return options;
            }
        }
    }
}

