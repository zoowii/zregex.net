﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZParser.Net
{
    public class CList<T>
    {
        public CList(){}
        public CList(T head, CList<T> tail)
        {
            this.Head = head;
            this.Tail = tail;
        }
        public T Head { get; protected set; }
        public CList<T> Tail
        {
            get;
            protected set;
        }
        protected CList<T> LastNode
        {
            get
            {
                var result = this;
                while (result.Tail != null)
                {
                    result = result.Tail;
                }
                return result;
            }
        }
        public bool HasNext
        {
            get
            {
                return this.Tail != null;
            }
        }
        public virtual CList<T> Cons(T item)
        {
            return new CList<T>(item, this);
        }
        public virtual CList<T> Append(T item)
        {
            var result = this.CloneNew();
            result.LastNode.Tail = new CList<T>(item, null);
            return result;
        }
        public virtual CList<T> CloneNew()
        {
            return new CList<T>(Head, this.Tail!=null?this.Tail.CloneNew():null);
        }
        public int Count
        {
            get
            {
                var cur = this;
                var sum = 1;
                while (cur.Tail != null)
                {
                    sum += 1;
                    cur = cur.Tail;
                }
                return sum;
            }
        }
        public T this[int index]
        {
            get
            {
                var count = this.Count;
                if(index<0 || index >= count)
                {
                    throw new IndexOutOfRangeException();
                }
                var cur = this;
                var pos = 0;
                while (pos < index)
                {
                    pos += 1;
                    cur = cur.Tail;
                }
                return cur.Head;
            }
        }
        public void ForEach(Action<T> action)
        {
            var cur = this;
            do
            {
                action(cur.Head);
                cur = cur.Tail;
            } while (cur != null);
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            this.ForEach((T item) =>
            {
                builder.Append(item.ToString());
            });
            return builder.ToString();
        }
    }
}
