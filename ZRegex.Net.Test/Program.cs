﻿using System;
using ZRegex.Net;
using NUnit.Framework;
using ZParser.Net;

namespace ZRegex.Net.Test
{
    [TestFixture]
    public class ZRegexTestUnit
    {
        [Test]
        public void TestRegexEngine()
        {
            var a = new CharExpr('a');
            var b = new CharExpr('b');
            var c = new CharExpr('c');
            var chars = new AlphaExpr();
            var digit = new DigitExpr();
            var expr = a.Plus().Concat(b.Union(c).Plus()).Concat(digit.Plus().Optional());
            var expr2 = a.Plus().Concat(b.Union(c).Repeat(3, 5));
            var bc = b.Union(c);
            var expr3 = a.Plus().Concat(bc.Repeat(3, 5));
            var expr4 = b.Concat(b).Concat(b.Optional());
            var iden = chars.Plus().Concat((chars.Union(digit)).Closure());
            iden.MarkGroup("identity");
            var ops = CharExpr.CreateUnionFromChars("+-*/=");
            ops.MarkGroup("operation");

            var expr5 = chars.Plus().Concat(digit.Plus());
            expr5.Build();
            expr4.Build();
            expr3.Build();
            expr2.Build();
            expr.Build();
            Console.WriteLine(expr);
            Console.WriteLine(expr4);
            var r1 = expr.Match("aaaacb345dddd");
            Console.WriteLine(r1);
            Assert.IsTrue(r1.Equals(MatchResult.of(false, new StringRegexCharString("aaaacb345"))));
            var r2 = expr4.Match("bb");
            Console.WriteLine(r2);
            Assert.IsTrue(r2.Equals(MatchResult.of(true, new StringRegexCharString("bb"))));
            var r3 = expr3.Match("aaabcb");
            Console.WriteLine(r3);
            Assert.IsTrue(r3.Equals(MatchResult.of(true, new StringRegexCharString("aaabcb"))));
            var r4 = expr2.Match("aaabcbc123");
            Console.WriteLine(r4);
            Assert.IsTrue(r4.Equals(MatchResult.of(false, new StringRegexCharString("aaabcbc"))));
            var r5 = expr5.Match("zoOwIi1992@NJU");
            Console.WriteLine(r5);
            Assert.IsTrue(r5.Equals(MatchResult.of(false, new StringRegexCharString("zoOwIi1992"))));
            var r6 = iden.Union(ops).MatchAll("def fib(n) n = n + 1 end");
            Console.WriteLine(r6);
            Assert.AreEqual(r6.Count, 8);
        }

        [Test]
        public void TestSimpleGroup()
        {
            var a = new EmptyExpr().Concat(new CharExpr('a')).Concat(new EmptyExpr());
            a.MarkGroup("a");
            var b = new EmptyExpr().Concat(new CharExpr('b')).Concat(new EmptyExpr());
            b.MarkGroup("b");
            var any = new CharNotInRangeExpr("ab"); // FIXME: if using new AnyCharExpr(), there will be bug of group feature
            any.MarkGroup("char");
            var expr = RegexExpr.UnionAll(a, b, any);
            expr.Build();
            var str = "abcdaebf";
            var res = expr.MatchAll(str);
            Console.WriteLine(res);
        }

        [Test]
        public void TestRegexStringReader()
        {
            var expr1 = RegexReader.Read(@"(a{3})(b+)(([c\s\.\d\\\+\u1234])*)");
            expr1.Build();
            var r1 = expr1.Match("aabbbc 123+556end");
            var r2 = expr1.Match("aaabbbc 123+556");
            // var expr2 = RegexReader.Read(@"abc");
            Console.WriteLine(expr1);
            Console.WriteLine(r1);
            Console.WriteLine(r2);
            Assert.IsFalse(r1.Matched);
            Assert.IsTrue(r2.Matched);
        }
    }

    [TestFixture]
    public class ZParserTestUnit
    {
        [Test]
        public void TestSimpleParser()
        {
            // a + b * c, symbol, +, *
            // E => E * E | E + E | (E) | I, I => symbol
            var EVar = new Var("E");
            var iVar = new Var("I");
            var mulVar = new FinalVar("*");
            var addVar = new FinalVar("+");
            var leftVar = new FinalVar("(");
            var rightVar = new FinalVar(")");
            var symbolVar = new FinalVar("Symbol");
            var rule1 = new Rule()
            {
                DestVar = EVar,
                Items = { 
					new RuleItem(){Items={leftVar, EVar, rightVar}},
                    new RuleItem(){Items={iVar}},
					new RuleItem(){Items={EVar, mulVar, EVar}},
					new RuleItem(){Items={EVar, addVar, EVar}}
				}
            };
            var rule2 = new Rule()
            {
                DestVar = iVar,
                Items = {
					new RuleItem(){Items={symbolVar}}
				}
            };
            var parser = new Parser() { Rules = { rule1, rule2 }, StartVar = EVar, Vars = { EVar, iVar, mulVar, addVar, leftVar, rightVar, symbolVar } };
            var tokens = TokenList.Create(
                new Token() { Text = "a", TokenType = symbolVar },
                new Token() { Text = "+", TokenType = addVar },
                new Token() { Text = "(", TokenType = leftVar },
                new Token() { Text = "b", TokenType = symbolVar },
                new Token() { Text = "*", TokenType = mulVar },
                new Token() { Text = "c", TokenType = symbolVar },
                new Token() { Text = ")", TokenType = rightVar }
            );
            var syntaxTree = parser.Parse(tokens);
            Console.WriteLine(syntaxTree);
            Assert.IsTrue(syntaxTree.ToString().Equals("a + ( b * c )"));
        }
        [Test]
        public void TestFailParser()
        {
            // a + b * c, symbol, +, *
            // E => E * E | E + E | (E) | I, I => symbol
            var EVar = new Var("E");
            var iVar = new Var("I");
            var mulVar = new FinalVar("*");
            var addVar = new FinalVar("+");
            var leftVar = new FinalVar("(");
            var rightVar = new FinalVar(")");
            var symbolVar = new FinalVar("Symbol");
            var rule1 = new Rule()
            {
                DestVar = EVar,
                Items = { 
					new RuleItem(){Items={leftVar, EVar, rightVar}},
                    new RuleItem(){Items={iVar}},
					new RuleItem(){Items={EVar, mulVar, EVar}},
					new RuleItem(){Items={EVar, addVar, EVar}}
				}
            };
            var rule2 = new Rule()
            {
                DestVar = iVar,
                Items = {
					new RuleItem(){Items={symbolVar}}
				}
            };
            var parser = new Parser() { Rules = { rule1, rule2 }, StartVar = EVar, Vars = { EVar, iVar, mulVar, addVar, leftVar, rightVar, symbolVar } };
            var tokens = TokenList.Create(
                new Token() { Text = "a", TokenType = symbolVar },
                new Token() { Text = "+", TokenType = addVar },
                new Token() { Text = "(", TokenType = leftVar },
                new Token() { Text = "b", TokenType = symbolVar },
                new Token() { Text = "*", TokenType = mulVar },
                new Token() { Text = "c", TokenType = symbolVar }
            );
            var syntaxTree = parser.Parse(tokens);
            Console.WriteLine(syntaxTree);
            Assert.IsNull(syntaxTree);
        }
    }
}
